/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.NewGen.labtest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
    
public class OxUnitTest {
    
    public OxUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow1_O_output_true(){
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow3_O_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow3_O_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","-"}};
        String curryPlayer = "O";
        boolean result = OX.checkWin(table, curryPlayer);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinCol1_O_output_true(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
        @Test
    public void testCheckWinCol0_O_output_true(){
        String[][] table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
        @Test
    public void testCheckWinCol2_O_output_true(){
        String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
     @Test
    public void testCheckWinDiagonal1_X_output_true(){
        String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinDiagonal2_X_output_true(){
        String[][] table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow2_X_output_true(){
    String[][] table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(table, currentPlayer);
    assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow1_X_output_true(){
        String[][] table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow3_X_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol1_X_output_true(){
        String[][] table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinCol0_X_output_true(){
        String[][] table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_X_output_true(){
        String[][] table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinDiagonal3_X_output_true(){
        String[][] table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinDiagonal4_X_output_true(){
        String[][] table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
}